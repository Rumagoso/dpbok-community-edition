#!/bin/bash

mkdir -p output/html/images

# section-level images
mkdir -p images
yes|cp -P bok/*/images/*.png images/
yes|cp -P bok/*/images/*.png output/html/images/
yes|cp -P bok/*/images/*.jpg images/
yes|cp -P bok/*/images/*.jpg output/html/images/

# chapter-level images
yes|cp -P bok/*/*/images/*.png images/
yes|cp -P bok/*/*/images/*.png output/html/images/
yes|cp -P bok/*/*/images/*.jpg images/
yes|cp -P bok/*/*/images/*.jpg output/html/images/

exit 0
