#!/bin/bash
# add h7 styling , unnumbered heading but colored
sed -e 's/^h6{font-size:1em}$/h6,h7{font-size:1em} \
h7{color:#ba3925;font-style:italic;line-height:2em}/g'
