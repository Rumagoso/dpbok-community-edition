
[[KLP-lean-product-dev]]
==== Lean Product Development

*Description*

One of the challenges with applying Lean to IT (as noted xref:KLP-lean-background[previously]) is that many IT professionals (especially software developers) believe that manufacturing is a “deterministic” field, whose lessons don't apply to developing technical products. “Creating software is like creating art, not being on an assembly line”, is one line of argument.

(((product development, distinguished from production))) (((production, distinguished from product development))) The root cause of this debate is the distinction between product development and production. It is true that an industrial production line - for example, producing forklifts by the thousands - may be repetitive. But how did the production line come to be? How was the forklift invented, or developed? It was created as part of a process of product development. It took mechanical engineering, electrical engineering, chemistry, materials science, and more. Combining fundamental engineering principles and techniques into a new, marketable product is not a repetitive process; it is a highly variable, creative process, and always has been.

(((innovation))) One dead end that organizations keep pursuing is the desire to make _((R&D))_ more “predictable"; that is, to reduce variation and predictably create innovation. This never works well; game-changing innovations are usually complex responses to complex market systems dynamics, including customer psychology, current trends, and many other factors. The process of innovating cannot, by its very nature, be made repeatable.

Developing innovative products and services drives the enterprise's growth. The future enterprise's performance is largely determined by the quality of product development. Products and services that fit market needs generate more profitable growth. Designing efficient product delivery processes determines up to 70% of your run or production costs.

Lean Product and Process Development (or LPPD) is not just applying Lean tools from the manufacturing floor to the LPPD environment. It is a unique set of principles, methods, and tools that build on the experience of enterprises such as Toyota, Ford, or Harley-Davidson.

(This section based on cite:[Morgan2018, Osterwalder2010, Morgan2006, Ward2014].)

The key characteristics of LPPD are:

* Clear definition of value from a customer perspective to inform product development from start to finish
* Chief Engineer system that integrates cross-functional expertise to architect a product that delivers value to customers and contributes to the economic success of the enterprise
* Front-loading the development process to explore thoroughly alternative solutions while there is maximum design space
* Set-Based Concurrent Engineering (SBCE) to facilitate the smooth integration of products' components
* High degree of teamwork facilitated by the Obeya process
* Knowledge and responsibility-based approach with planned learning cycles
* Levelled workload through Cadence, Pull, and Flow

===== Define Value from a Customer Perspective

One of the Lean Product Development practices is "Go & See". Instead of relying on secondary information such as market studies or marketing reports (as also discussed in xref:KLP-prod-discovery-techniques[Product Discovery]), product team members are encouraged to experience first hand customers' needs, problems, and emotions. For example, one of the ((Toyota)) Chief Engineers rented a car in Canada and drove for several months in the winter time to understand the unique needs of the Canadian driver. Design thinking possibly combined with anthropological approaches help understand value from a customer's perspective.

// this is more about "what" not "how" and so arguably goes into Chapter 4. We may need to rationalize Chapter 4 _versus_ 5.

===== The Chief Engineer System

Toyota's Chief Engineers are not program managers who focus on controlling and reporting development activities. They are leaders who create and communicate a compelling and feasible vision. They define a clear and logical architecture for the product and value stream. Their T-shaped profile gives them enough understanding of the various disciplines at play so they can help solve cross-disciplinary problems. Chief Engineers are accountable for the economic success of their products. Last but not the least, their leadership skills help them inspire excellent engineers. The ((Chief Engineer)) does not have formal authority on the teams that develop the product. Team members report to functional department heads. New Agile at scale organizational models such as the xref:spotify-model[Spotify model] are similar with teams members reporting to chapters or guilds and not squad leaders. The Product Owner plays a role comparable to the Chief Engineer's one at a smaller scale.

===== Front Load the Development Process

Poor decisions made early in the development process have negative consequences that increase exponentially over time because reversing them later in the lifecycle becomes more and more difficult. Amazon CEO Jeff Bezos classifies decisions into type 1 and type 2 categories cite:[Bezos2016]. Type 1 decisions are not reversible, and you have to be very careful making them. Type 2 decisions are like walking through a door — if you don't like the decision, you can always go back. Because type 1 decisions are difficult to reverse, alternatives should be thoroughly explored before the final decision is made. This tendency to front load the development process could slow the development process.

===== Set-Based Concurrent Engineering

((Set-Based Concurrent Engineering)) or ((SBCE)) makes front loading compatible with short product development lead times. Instead of focusing on the rapid completion of individual component designs in isolation, SBCE looks at how individual designs will interact within a system before the design is complete. The focus is on system integration before individual design completion. The concurrent nature of the design process contributes to shortening product development lead time while front loading combined with the integration focus helps minimize bad design decisions which would at the end slow the development process and increase "non-quality". A good metaphor for SBCE is doodle.com which offers a much better way of scheduling a meeting compared to the old iterative "point-based" way of finding a time that works for all.

===== The Obeya Process

We introduced the concept of xref:andon[Andon] previously. Lean Product Development has a similar practice, _Obeya_. The ((Obeya)) process begins with the entire team posting in a physical room visual artifacts representing the product's components. Product component owners are responsible for posting status information such as timing, issues, key design questions, etc. Because the information is shared in a transparent manner, useful conversations are elicited. When problems are identified they are analyzed using problem solving approaches such as PDCA and A3. Collocation greatly intensifies communication and helps solve problems earlier. One of the advantages of the Obeya process is that it does not force the enterprise to change its departmental organization or to co-locate hundreds of engineers. When an Obeya room cannot be set up at the same place, virtual ones can be created using specialized collaborative software. The Obeya process proved to be a critical element of the Toyota product development system helping radically reduce lead time.

===== Knowledge and Responsibility-Based

Traditional task-based project management is based on tasks completed and not on technical results. Because project managers do not understand the reality that hides behind the Gantt chart, problems can remain hidden for a long time. In contrast, the Chief Engineer defines integrating events at fixed dates. Required results are communicated to responsible engineers who are free to plan and organize as needed to meet these dates and deliver expected results. Top-down detailed planning and control is replaced by top-down objectives, the detailed planning and execution being delegated to autonomous teams. The responsibility style helps develop a learning development organization. Bureaucracy is eliminated and the creation of useful knowledge encouraged.

===== Levelled Workload through Cadence, Pull, and Flow

Unevenness (Mura) and overburden (Muri) are root causes of waste (Muda) in both production and development value streams. In the context of LPPD work should be released in the organization on a regular xref:cadence[cadence] in order to level the workload. Integrating events gives freedom to developers to plan their work to meet those events. In this way development work is pulled (as covered in the xref:KLP-lean-work-mgmt[Kanban] discussion) rather than scheduled. Similarly information is pulled by developers based on what they need to know rather than being pushed according to some centrally planned schedule. Don Reinertsen, the author of _The Principles of Product Development Flow_, proposes a method to maximize the economic benefit of a portfolio of projects. The key idea is that the sequencing of projects should consider both the xref:cost-of-delay[cost of delay] of each project and the amount of time that the project will block scarce development resources. This approach is known as a Weighted Shortest Job First (WSJF) queueing discipline. It has influenced the Agile community; SAFe specifies WSJF to prioritize backlogs.

===== Reinertsen's Product Flow Model

In IT, simply developing software for a new problem (or even new software for an old problem) is an R&D problem, not a production line problem. It is iterative, uncertain, and risky, just like other forms of product development. That does not mean it is completely unmanageable, or that its creation is a mysterious, artistic process. It is just a more variable process with a higher chance of failure, and with a need to incorporate feedback quickly to reduce the risk of xref:open-loop[open-loop](((open-loop))) control failure. These ideas are well known to the Agile community and its authors. However, there is one thought leader who stands out in this field: an ex-Naval officer and nuclear engineer named Donald Reinertsen who was introduced in our previous discussions on xref:KLP-beneficial-variability[beneficial variability in product discovery] and xref:queuing[queuing].

Reinertsen's work dates back to 1991, and (originally as a co-author with Preston G. Smith) presaged important principles of the Agile movement cite:[Smith1991], from the general perspective of product development. Reinertsen's influence is well documented and notable. He was partnering with David Anderson when Anderson created the “software Kanban” approach. He wrote the introduction to Leffingwell's _Agile Software Requirements_, the initial statement of ((SAFe)). His influence is pervasive in the Agile community. His work is deep and based on fundamental mathematical principles such as queueing theory. His work can be understood as a series of interdependent principles:

* The flow or throughput of product innovation is the primary driver of financial success (notice that innovation must be *accepted by the market* — simply producing a new product is not enough)
* Product development is essentially the creation of information
* The creation of information requires fast xref:feedback[feedback]
* Feedback requires limiting work-in-process
* Limiting work-in-process in product design contexts requires rigorous prioritization capabilities
* Effective, economical prioritization requires understanding the http://www.leadingagile.com/2015/06/an-introduction-to-cost-of-delay/[cost of delay] for individual product features
* Understanding cost of delay requires smaller batch sizes, consisting of cohesive features, not large projects (this supporting point to Reinertsen's work was introduced by Josh Arnold cite:[Arnold2013])

These can be summarized as in <<fig-pyramid-400-o>>.

[[fig-pyramid-400-o]]
.Lean Product Development Hierarchy of Concerns
image::images/2_05-pyramid.png[pyramid, 400,,float="right"]

If a company wishes to innovate faster than competitors, it requires fast xref:feedback[feedback](((feedback))) on its experiments (whether traditionally understood, laboratory-based experiments, or market-facing validation as in xref:KLP-lean-startup[Lean Startup](((Lean Startup)))). In order to achieve fast feedback, xref:work-in-process[work-in-process](((work-in-process))) _must_ be reduced in the system, otherwise xref:queuing[high-queue states](((high-queue states))) will slow feedback down.

But how do we reduce work-in-process? We have to _prioritize_. Do we rely on the xref:KLP-hippo[HiPPO](((HiPPO))), or do we try something more rational? This brings us to the critical concept of _((cost of delay))_.

[[cost-of-delay]]
===== Cost of Delay

Don Reinertsen is well known for advocating the concept of “cost of delay” in understanding product economics. The term is intuitive; it represents the loss experienced by delaying the delivery of some value. For example, if a delayed product misses a key trade show, and therefore its opportunity for a competitive xref:KLP-release-mgmt[release], the cost of delay might be the entire addressable market. Understanding cost of delay is part of a broader economic emphasis that Reinertsen brings to the general question of product development. He suggests that product developers, in general, do not understand the fundamental economics of their decisions regarding resources and work-in-process.

In order to understand the cost of delay, it is first necessary to think in terms of a market-facing product (such as a smartphone application). Any market-facing product can be represented in terms of its lifecycle revenues and profits (see <<tbl-lifecycle-table>>, <<fig-lifecycle-graph-500-c>>).

[[tbl-lifecycle-table]]
.Product Lifecycle Economics by Year
[cols="<,>,>,>,>", options="header"]
|====

^|Year
^|Annual Cost
^|Annual Revenue
^|Annual Profit
^|Cumulative Profit

|**Year 1**|100|0|&#8211;100|&#8211;100
|**Year 2**|40|80|40|&#8211;60
|**Year 3**|30|120|90|30
|**Year 4**|25|150|125|155
|**Year 5**|25|90|65|220
|**Year 6**|20|60|40|260
|====

[[fig-lifecycle-graph-500-c]]
.Product Lifecycle Economics, Charted
image::images/2_05-lifecycle-graph.png[cost of delay graph, 500]

(((cost of delay, examples))) The numbers above represent a product lifecycle, from R&D through production to retirement. The first year is all cost, as the product is being developed, and net profits are negative. In year 2, a small net profit is shown, but cumulative profit is still negative, as it remains in year 3. Only into year 3 does the product break even, ultimately achieving lifecycle net earnings of 175. But what if the product's introduction into the market is delayed? The consequences can be severe.

Simply delaying delivery by a year, all things being equal in our example, will reduce lifecycle profits by 30% (see <<tbl-lifecycle-tableB>>, <<fig-lifecycle-graphB-500-c>>).

ifdef::backend-pdf[<<<]
[[tbl-lifecycle-tableB]]
.Product Lifecycle, Simple Delay
[cols="<,>,>,>,>", options="header"]
|====

^|Year
^|Annual Cost
^|Annual Revenue
^|Annual Profit
^|Cumulative Profit

|**Year 1**|100|0|&#8211;100|&#8211;100
|**Year 2**|40|0|&#8211;40|&#8211;140
|**Year 3**|30|80|50|&#8211;90
|**Year 4**|25|120|95|5
|**Year 5**|25|150|125|130
|**Year 6**|20|90|70|200
|====

[[fig-lifecycle-graphB-500-c]]
.Product Lifecycle, Simple Delay, Charted
image::images/2_05-lifecycle-graphB.png[cost of delay graph, 500]

But all things are not equal. What if, in delaying the product for a year, we allow a competitor to gain a superior market position? That could depress our sales and increase our per-unit costs — both bad (see <<tbl-lifecycle-tableC>>, <<fig-lifecycle-graphC-500-c>>).

// break page for pdf
ifdef::backend-pdf[<<<]
[[tbl-lifecycle-tableC]]
.Product Lifecycle, Aggravated Delay
[cols="<,>,>,>,>", options="header"]
|====

^|Year
^|Annual Cost
^|Annual Revenue
^|Annual Profit
^|Cumulative Profit

|**Year 1**|100|0|&#8211;100|&#8211;100
|**Year 2**|40|0|&#8211;40|&#8211;140
|**Year 3**|35|70|35|&#8211;105
|**Year 4**|30|100|70|&#8211;35
|**Year 5**|30|120|90|55
|**Year 6**|25|80|55|110
|====

[[fig-lifecycle-graphC-500-c]]
.Product Lifecycle, Aggravated Delay, Charted
image::images/2_05-lifecycle-graphC.png[cost of delay graph, 500]

The advanced cost of delayed analysis argues that different product lifecycles have different characteristics. Josh Arnold of Black Swan Farming has visualized these as a set of profiles cite:[Arnold2013]. See <<fig-simpleCOD-400-c>> (similar to cite:[Arnold2013]) for the simple delay profile.

[[fig-simpleCOD-400-c]]
.Simple Cost of Delay
image::images/2_05-simpleCOD.png[simple delay curve,400,]

In this delay curve, while profits and revenues are lost due to late entry, it is assumed that the product will still enjoy its expected market share. We can think of this as the “iPhone _versus_ Android” profile, as Android was later but still achieved market parity. The aggravated cost of delay profile, however, looks like <<fig-aggroCOD-400-c>> (similar to cite:[Arnold2013]).

[[fig-aggroCOD-400-c]]
.Aggravated Cost of Delay
image::images/2_05-aggroCOD.png[aggravated delay curve,400,]

In this version, the failure to enter the market in a timely way results in long-term loss of market share. We can think of this as the “Amazon Kindle(TM) _versus_ Barnes & Noble Nook” profile, as the Nook has not achieved parity, and does not appear likely to. There are other delay curves imaginable, such as delay curves for tightly time-limited products (e.g., such as found in the fashion industry) or cost of delay that is only incurred after a specific date (such as in complying with a regulation).

Reinertsen observes that product managers may think that they intuitively understand cost of delay, but when he asks them to estimate the aggregate cost of (for example) delaying their product's delivery by a given period of time, the estimates provided by product team participants *in a position to delay delivery* may vary by up to 50:1. This is powerful evidence that a more quantitative approach is essential, as opposed to relying on “gut feel” or the xref:KLP-hippo[HiPPO].

((("Arnold, Josh"))) Finally, Josh Arnold notes that cost of delay is much easier to assess on small batches of work. Large projects tend to attract many ideas for features, some of which have stronger economic justifications than others. When all these features are lumped together, it makes understanding the cost of delay a challenging process, because it then becomes an average across the various features. But since features, ideally, can be worked on individually, understanding the cost of delay at that level helps with the prioritization of the work.

The combination of xref:KLP-product-roadmapping[product roadmapping], a high-quality xref:KLP-scrum[DEEP backlog], and cost of delay is a solid foundation for digital product development. It is essential to have an economic basis for making the ((prioritization)) decision. Clarifying the economic basis is a critical function of the product roadmap. Through estimation of story points, we can understand the team's velocity. Estimating velocity is key to planning, which we will discuss further in xref:chap-invest-mgmt[]. Through understanding the economics of product availability to the market or internal users, the cost of delay can drive backlog prioritization.

*Evidence of Notability*

Lean influences on software development and the management of digital systems are the subject of conference talks, books, and articles, and much other evidence demonstrating an engaged community of interest. Notable works include cite:[Kim2013, Anderson2010, Poppendieck2007, Bell2010, Reinertsen2009].

*Limitations*

Lean has broad applicability but the nature of the digital work must be understood carefully. Classic Lean applies well to less-variable operational work in digital systems. Developing new digital systems requires Lean Product Development principles, and some aspects of classic Lean (e.g., always reducing variability) are less applicable or may even be harmful. See, for example, cite:[Reinertsen2009] for further discussion (Chapter 4, "The Economics of Product Development Variability").



*Related Topics*

* xref:KLP-app-deliv[Application Delivery]
* xref:KLP-product-team-approaches[Product Team Practices]
* xref:KLP-lean-work-mgmt[Lean Management]
* xref:KLP-chap-coordination[Coordination and Process]
* xref:KLP-organization[Organizational Structure]
