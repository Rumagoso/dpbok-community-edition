
ifdef::backend-pdf[<<<]
[[KLP-digital-lifecycle]]
==== The Digital Lifecycle

[[KLP-essential-states]]
===== The Essential States of the Digital Product

*Description*

[[fig-ITStates-600-c]]
.The Essential States of the Digital Product
image::images/1_01-ITStates.png[IT States, 600]

The digital or IT service is based on a complex stack of technology, from local devices to global networks to massive data centers. Software and hardware are layered together in endlessly inventive ways to solve problems people did not even know they had ten years ago. However, these IT service systems must come from somewhere. They must be designed, built, and operated, and continually improved over time. A simple representation of the IT service lifecycle is:

* An idea is developed for an IT-enabled value proposition that can make a profit, or better fulfill a mission
* The idea must garner support and resources so that it can be built
* The idea is then constructed, at least as an initial proof of concept or MVP (construction is assumed to include an element of design; in this document, design and construction are not represented as two large-scale separate phases; the activities may be distinct, but are conducted within a context of faster design-build iterations)
* There is a critical state transition, however, that will always exist; initially, it is the change from OFF to ON when the system is first constructed - after the system is ON, there are still distinct changes in state when new features are deployed, or incorrect behaviors ("bugs" or "defects") are rectified
* The system may be ON, but it is not delivering value until the user can access it; sometimes, that may be as simple as providing someone with a network address, but usually there is some initial "provisioning" of system access to the user, who needs to identify themselves
* The system can then deliver services (moments of truth) to the end users; it may deliver millions or billions of such experiences, depending on its scale and how to count the subjective concept of value experience
* The user may have access, but may still not receive value, if they do not understand the system well enough to use it; whether via a formal service desk, or informal social media channels, users of IT services will require and seek support on how to maximize the value they are receiving from the system
* Sometimes, something is wrong with the system itself; if the system is no longer delivering value experiences (bank balances, restaurant reservations, traffic directions) then some action must be taken promptly to restore service
* All of the previous states in the lifecycle generate data and insight that lead to further evolution of the system; there is a wide variety of ways systems may evolve: new user functionality, more stable technology, increased system capacity, and more - such motivations result in new construction and changes to the existing system, and so the cycle begins again
* Unless ... the system's time is at an end; if there is no reason for the system to exist any longer, it should be retired

(((lifecycle, service)))The digital service/product evolves over time, through many repetitions ("iterations") of the improvement cycle. An expanding spiral is a useful visualization:

[[fig-ITSvcChg-800-c]]
.The Digital Service Lifecycle
image::images/1_01-ITSvcChg.png[IT service changes, 800]

This entire process, from idea to decommissioning (“inspire to retire”) can be understood as the _service lifecycle_ (see <<fig-ITSvcChg-800-c>>). Sometimes, the service lifecycle is simplified as "plan, build, run"; however, this can lead to the assumption that only one iteration is required, which is in general incorrect in digital systems. Multiple iterations should be assumed as the product is fine-tuned and evolves to meet changing demand.

[[dual-axis-vc]]

We can combine the service experience (moment of truth) with the service/product lifecycle into the “dual-axis value chain” (originally presented in cite:[Betz2011]):

[[fig-dual-axis-400-o]]
.Dual-Axis Value Chain
image::images/1_01-2-axis-value-chain.png[dual axis value chain, 450, float="right"]

The dual-axis value chain (((dual-axis value chain))) can be seen in many representations of IT and digital delivery systems. Product evolution flows from right to left, while day-to-day value flows up, through the technical stack. It provides a basis for (among other things) thinking about the IT user, customer, and sponsor, which we will cover in the next section.

[[KLP-3-ways-devops]]
===== The Three Ways of DevOps

xref:KLP-devops-technical-practices[DevOps] is discussed in depth later in this document. At this point, however, as originally conceived in _The Phoenix Project_ cite:[Kim2013], there are three core DevOps principles applicable at the earliest stages of the digital product:

* Flow (the "First Way")
* Feedback (the "Second Way")
* Continuous Learning (the "Third Way")

DevOps emphasizes speeding up the flow of value in the product lifecycle (left to right), and the feedback of learning (right to left) "at all stages of the value stream". When this is done consistently at scale over time, DevOps advocates argue that the result is a: "generative, high-trust culture that supports a dynamic, disciplined, and scientific approach to experimentation and risk-taking, facilitating the creation of organizational learning, both from our successes and failures. Furthermore, by continually shortening and amplifying our feedback loops, we create ever-safer systems of work and are better able to take risks and perform experiments that help us learn faster than our competition and win in the marketplace." cite:[Kim2016(12)].


*Limitations*

The limitations of relying on a lifecycle model as a basis for systems implementation are by now well known. Attempting to fully understand a system's "requirements" prior to any development can lead to project failure, and deferring integration of sub-modules until late in a project also is risky. See Agile canon for further discussion (e.g., cite:[Schwaber2002, Poppendieck2003, Reinertsen2009, Cohn2010]). Nevertheless, the concept of the lifecycle remains a useful model; these state transitions exist and drive organizational behavior.

*Evidence of Notability*

The evidence for this topic's importance is seen across much guidance for software and systems professionals. Examples include the original statement of "waterfall" development cite:[Royce1970], the overlapping phases of the Rational Unified Process cite:[IBM2011], the ITIL Service Lifecycle's "strategy/design/transition/operate/improve," cite:[TSO2011], the clear reach and influence of the DevOps movement, and many more.

*Related Topics*

* xref:KLP-app-deliv[Application Development]
* xref:KLP-CA-product-mgmt[Product Management]
* xref:KLP-work-management[Work Management]
* xref:KLP-ops-mgmt[Operations Management]
